import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix

dataset = pd.read_csv("dataR2.csv")

X = dataset.drop('Classification', axis=1)
y = dataset['Classification']


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.7, random_state=61)
X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.2, random_state=61)


svc = SVC(kernel='poly', gamma='scale', C=1e2)
svc.fit(X_train, y_train)

prediction = svc.predict(X_test)
print(confusion_matrix(y_test, prediction))
print(classification_report(y_test, prediction))
