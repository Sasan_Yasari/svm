import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix

def svcClassificationReport(kernel, X_train, y_train, X_test, y_test, c):
	svc = SVC(kernel=kernel, gamma='scale', C=c)
	svc.fit(X_train, y_train)

	prediction = svc.predict(X_test)
	print('---------------------' + kernel + '---------------------------')
	print(confusion_matrix(y_test, prediction))
	print(classification_report(y_test, prediction))
	print('------------------------------------------------------')


dataset = pd.read_csv("dataR2.csv")

X = dataset.drop('Classification', axis=1)
y = dataset['Classification']


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.7, random_state=61)
X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.2, random_state=61)

svcClassificationReport('poly', X_train, y_train, X_test, y_test)
svcClassificationReport('rbf', X_train, y_train, X_test, y_test)
svcClassificationReport('sigmoid', X_train, y_train, X_test, y_test)