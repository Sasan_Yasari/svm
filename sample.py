import numpy as np
import matplotlib.pyplot as plt
from cvxopt import matrix as cvxopt_matrix
from cvxopt import solvers as cvxopt_solvers

x_neg = np.array([[1,1],[1,-1],[-1,1],[-1,3],[2,2],[-1,3]])
y_neg = np.array([-1,-1,-1,-1,-1,-1])
x_pos = np.array([[3,4],[5,6],[6,5],[4,5],[7,10]])
y_pos = np.array([1,1,1,1,1])
x1 = np.linspace(-10,10)
x = np.vstack((np.linspace(-10,10),np.linspace(-10,10)))


#New dataset (for later)
X = np.array([[1,1],[1,-1],[-1,1],[-1,3],[2,2],[-1,3], [3,4],[5,6],[6,5],[4,5],[7,10]] )
y = np.array([-1,-1, -1, -1, -1, 1, 1 , 1, 1 ,1, 1])


C = 10
m,n = X.shape
y = y.reshape(-1,1) * 1.
X_dash = y * X
H = np.dot(X_dash , X_dash.T) * 1.

#Converting into cvxopt format - as previously
P = cvxopt_matrix(H)
q = cvxopt_matrix(-np.ones((m, 1)))
G = cvxopt_matrix(np.vstack((np.eye(m)*-1,np.eye(m))))
h = cvxopt_matrix(np.hstack((np.zeros(m), np.ones(m) * C)))
A = cvxopt_matrix(y.reshape(1, -1))
b = cvxopt_matrix(np.zeros(1))

#Run solver
sol = cvxopt_solvers.qp(P, q, G, h, A, b)
alphas = np.array(sol['x'])

#==================Computing and printing parameters===============================#
w = ((y * alphas).T @ X).reshape(-1,1)
S = (alphas > 1e-4).flatten()
b = y[S] - np.dot(X[S], w)

#Display results
print('Alphas = ',alphas[alphas > 1e-4])
print('w = ', w.flatten())
print('b = ', b[0])


exit()

plt.plot([1, 1, -1, -1, 2], [1, -1, 1, 3, 2], 'ro')
plt.plot([3, 5, 6, 4, 7], [4, 6, 5, 5, 10], 'bo')
# plt.plot([0], [17], 'go')

x = np.linspace(-1, 8, 1000)
# plt.plot(x, -.5*x + 19/4, '--c')
plt.plot(x, -.5*x + 17/4, ':r')
# plt.plot(x, -.5*x + 15/4, '--c')

plt.grid()
plt.savefig('1')



# import numpy as np
# import matplotlib.pyplot as plt
# from scipy import stats

# X = [[1, 1], [1, -1], [-1, 1], [-1, 3], [2, 2], [3, 4], [5, 6], [6, 5], [4, 5], [7, 10], [-1, 3]]
# y = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1]

# from sklearn.svm import SVC 
# model = SVC(kernel='linear', C=1E10)
# model.fit(X, y)


# def plot_svc_decision_function(model, ax=None, plot_support=True):
# 	if ax is None:
# 		ax = plt.gca()
# 	xlim = ax.get_xlim()
# 	ylim = ax.get_ylim()
    
# 	x = np.linspace(xlim[0], xlim[1], 30)
# 	y = np.linspace(ylim[0], ylim[1], 30)
# 	Y, X = np.meshgrid(y, x)
# 	xy = np.vstack([X.ravel(), Y.ravel()]).T
# 	P = model.decision_function(xy).reshape(X.shape)
    
# 	ax.contour(X, Y, P, colors='k',
# 				levels=[-1, 0, 1], alpha=0.5,
# 				linestyles=['--', '-', '--'])
    

# 	if plot_support:
# 		ax.scatter(model.support_vectors_[:, 0],
# 					model.support_vectors_[:, 1],
# 					s=300, linewidth=1, facecolors='none');
# 	ax.set_xlim(xlim)
# 	ax.set_ylim(ylim)
# 	plt.show()

# # plot_svc_decision_function(model)
# # plt.scatter([i[0] for i in X], [i[1] for i in X], c=y, s=50, cmap='autumn')
# # plt.show()
# w = model.coef_[0]
# print(w)
# print(model.intercept_[0])
